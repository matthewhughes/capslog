// Package capslog provides some [slog.Handler] that are meant to be used during
// testing to verify what was logged.
// It is inspired by the [caplog pytest fixture]
//
// [caplog pytest fixture]: https://docs.pytest.org/en/stable/reference/reference.html#std-fixture-caplog
package capslog

import (
	"context"
	"log/slog"
	"sync"
)

// CapturedRecord is a record captured by a [CaptureHandler].
// It's basically just a wrapper around a [slog.Record] that provides
// convenient access to any attributes or groups.
type CapturedRecord struct {
	Message string
	Level   slog.Level

	// Attrs is the list of attributes passed via WithAttrs or directly in a
	// call to a function like handler.Info (in that order). It will be `nil`
	// if no groups are set
	Attrs []slog.Attr

	// Groups is the list of groups assigned to a logger, i.e. the list of
	// names passed to WithGroup for all parent handlers of the current
	// handler. It will be `nil` if no groups are set
	Groups []string
}

// CaptureHandler is a [slog.Handler] that captures each record it returns.
//
// It will also capture the logs recorded by any child handler created from
// [CaptureHandler.WithAttrs] or [CaptureHandler.WithGroup]
type CaptureHandler struct {
	// CapturedRecords are the records captured by the handler, or any of
	// its children
	CapturedRecords []CapturedRecord
	level           slog.Level
	parent          *CaptureHandler
	attrs           []slog.Attr
	groups          []string
	mu              sync.Mutex
}

func NewCaptureHandler(level slog.Level) *CaptureHandler {
	return &CaptureHandler{level: level, mu: sync.Mutex{}}
}

func newCapturedRecord(record slog.Record, groups []string) CapturedRecord {
	var attrs []slog.Attr
	if record.NumAttrs() > 0 {
		attrs = make([]slog.Attr, 0, record.NumAttrs())
		record.Attrs(func(attr slog.Attr) bool {
			attrs = append(attrs, attr)
			return true
		})
	}

	return CapturedRecord{
		Message: record.Message,
		Level:   record.Level,
		Attrs:   attrs,
		Groups:  groups,
	}
}

func (h *CaptureHandler) captureRecord(record slog.Record, groups []string) {
	record.AddAttrs(h.attrs...)

	if h.parent != nil {
		h.parent.captureRecord(record, groups)
		return
	}

	h.mu.Lock()
	h.CapturedRecords = append(h.CapturedRecords, newCapturedRecord(record, groups))
	h.mu.Unlock()
}

func (h *CaptureHandler) Handle(_ context.Context, record slog.Record) error {
	h.captureRecord(record, h.groups)
	return nil
}

func (h *CaptureHandler) Enabled(_ context.Context, level slog.Level) bool {
	return level >= h.level
}

// WithAttrs returns a new handler whose records will be recorded, with the
// extra attributes added, by the parent
func (parent *CaptureHandler) WithAttrs(attrs []slog.Attr) slog.Handler {
	return &CaptureHandler{level: parent.level, parent: parent, attrs: attrs}
}

// WithGroup returns a new handler whose records will be recorded, with the
// given group name, by the parent
func (parent *CaptureHandler) WithGroup(name string) slog.Handler {
	return &CaptureHandler{level: parent.level, parent: parent, groups: append(parent.groups, name)}
}

// NullHandler is a [slog.Handler] that does nothing.
// That is, it is never enabled and never records anything, useful if you want
// avoid noisy logging in tests
type NullHandler struct{}

// NullLogger is a logger using the [NullHandler], that is, a logger that does
// nothing
var NullLogger = slog.New(&NullHandler{})

func (h *NullHandler) Handle(_ context.Context, _ slog.Record) error {
	return nil
}

func (h *NullHandler) Enabled(_ context.Context, _ slog.Level) bool {
	return false
}

func (h *NullHandler) WithAttrs(attrs []slog.Attr) slog.Handler {
	return &NullHandler{}
}

func (h *NullHandler) WithGroup(name string) slog.Handler {
	return &NullHandler{}
}
