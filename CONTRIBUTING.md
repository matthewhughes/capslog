# Contributing

## Set up

As well as a Go installation you will need:

  - [pre-commit](https://pre-commit.com/#install)

Testing is this done with:

    go test ./...

And linting via:

    pre-commit run --all-files
