package capslog_test

import (
	"fmt"
	"log/slog"

	"gitlab.com/matthewhughes/capslog"
)

func do_stuff(name string, logger *slog.Logger) {
	logger.Info("doing stuff", "name", name)
	// do stuff...
}

func ExampleCaptureHandler() {
	handler := capslog.NewCaptureHandler(slog.LevelInfo)
	logger := slog.New(handler)
	do_stuff("foo", logger)

	expected := capslog.CapturedRecord{
		Message: "doing stuff",
		Level:   slog.LevelInfo,
		Attrs: []slog.Attr{
			{
				Key:   "name",
				Value: slog.StringValue("foo"),
			},
		},
	}

	capturedRecords := handler.CapturedRecords
	fmt.Println(len(capturedRecords))
	fmt.Println(capturedRecords[0].Message == expected.Message)
	fmt.Println(capturedRecords[0].Level == expected.Level)
	attrs := capturedRecords[0].Attrs
	fmt.Println(len(attrs))
	fmt.Println(attrs[0].Key == expected.Attrs[0].Key)
	fmt.Println(attrs[0].Value.String() == expected.Attrs[0].Value.String())

	// Output:
	// 1
	// true
	// true
	// 1
	// true
	// true
}

func ExampleCaptureHandler_WithAttrs() {
	handler := capslog.NewCaptureHandler(slog.LevelInfo)
	child := handler.WithAttrs([]slog.Attr{{Key: "some-key", Value: slog.StringValue("value")}})
	logger := slog.New(child)
	do_stuff("foo", logger)

	capturedRecords := handler.CapturedRecords
	fmt.Println(len(capturedRecords))
	fmt.Println(capturedRecords[0].Message)
	fmt.Println(capturedRecords[0].Attrs)

	// logs are only captured by the parent
	fmt.Println(len(child.(*capslog.CaptureHandler).CapturedRecords))

	// Output:
	// 1
	// doing stuff
	// [name=foo some-key=value]
	// 0
}

func ExampleCaptureHandler_WithGroup() {
	handler := capslog.NewCaptureHandler(slog.LevelInfo)
	child := handler.WithGroup("my-group")
	logger := slog.New(child)
	do_stuff("foo", logger)

	capturedRecords := handler.CapturedRecords
	fmt.Println(len(capturedRecords))
	fmt.Println(capturedRecords[0].Message)
	fmt.Println(capturedRecords[0].Groups)

	// logs are only captured by the parent
	fmt.Println(len(child.(*capslog.CaptureHandler).CapturedRecords))

	// Output:
	// 1
	// doing stuff
	// [my-group]
	// 0
}

func ExampleNullHandler() {
	handler := &capslog.NullHandler{}
	logger := slog.New(handler)

	logger.Debug("testing")
	logger.Info("testing")
	logger.Warn("testing")
	logger.Error("testing")

	// Output:
}

func Example_nullLogger() {
	logger := capslog.NullLogger

	logger.Debug("testing")
	logger.Info("testing")
	logger.Warn("testing")
	logger.Error("testing")

	// Output:
}
