# Changelog

## 0.2.0 - 2024-04-27

### Added

  - Add `NullLogger`

## 0.1.0 - 2023-12-26

Initial release
