# `capslog`

[![coverage
report](https://gitlab.com/matthewhughes/capslog/badges/main/coverage.svg)]()
[![Go
Reference](https://pkg.go.dev/badge/gitlab.com/matthewhughes/capslog.svg)](https://pkg.go.dev/gitlab.com/matthewhughes/capslog)

A library to help assert what was logged by a `slog.Handler` during tests. See
the [docs](https://pkg.go.dev/gitlab.com/matthewhughes/capslog) for more info.

Like any output from your software, it can be useful to assert that what is
being logged is what we expect, and is indeed useful, helpful for the person
trying to debug things in the middle of the night.
