package capslog_test

import (
	"context"
	"log/slog"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/matthewhughes/capslog"
)

// interface assertions
var (
	_ slog.Handler = (*capslog.NullHandler)(nil)
	_ slog.Handler = (*capslog.CaptureHandler)(nil)
)

func TestCapture(t *testing.T) {
	for _, tc := range []struct {
		handler  *capslog.CaptureHandler
		logFunc  func(*slog.Logger, string, ...any)
		message  string
		args     []any
		expected []capslog.CapturedRecord
	}{
		{
			message: "log at debug",
			handler: capslog.NewCaptureHandler(slog.LevelDebug),
			logFunc: (*slog.Logger).Debug,
			expected: []capslog.CapturedRecord{
				{
					Message: "log at debug",
					Level:   slog.LevelDebug,
				},
			},
		},
		{
			message: "log at info",
			handler: capslog.NewCaptureHandler(slog.LevelInfo),
			logFunc: (*slog.Logger).Info,
			expected: []capslog.CapturedRecord{
				{
					Message: "log at info",
					Level:   slog.LevelInfo,
				},
			},
		},
		{
			message: "log at warn",
			handler: capslog.NewCaptureHandler(slog.LevelWarn),
			logFunc: (*slog.Logger).Warn,
			expected: []capslog.CapturedRecord{
				{
					Message: "log at warn",
					Level:   slog.LevelWarn,
				},
			},
		},
		{
			message: "log at error",
			handler: capslog.NewCaptureHandler(slog.LevelError),
			logFunc: (*slog.Logger).Error,
			expected: []capslog.CapturedRecord{
				{
					Message: "log at error",
					Level:   slog.LevelError,
				},
			},
		},
		{
			message:  "nothing logged when not enabled",
			handler:  capslog.NewCaptureHandler(slog.LevelWarn),
			logFunc:  (*slog.Logger).Info,
			expected: nil,
		},
		{
			message: "attrs included",
			args:    []any{"foo", "bar"},
			handler: capslog.NewCaptureHandler(slog.LevelInfo),
			logFunc: (*slog.Logger).Info,
			expected: []capslog.CapturedRecord{
				{
					Message: "attrs included",
					Level:   slog.LevelInfo,
					Attrs: []slog.Attr{
						{
							Key:   "foo",
							Value: slog.StringValue("bar"),
						},
					},
				},
			},
		},
	} {
		t.Run(tc.message, func(t *testing.T) {
			logger := slog.New(tc.handler)
			tc.logFunc(logger, tc.message, tc.args...)

			require.Equal(t, tc.expected, tc.handler.CapturedRecords)
		})
	}
}

func TestWithAttrs(t *testing.T) {
	attrs := []slog.Attr{{Key: "my-key", Value: slog.StringValue("value")}}
	baseHandler := capslog.NewCaptureHandler(slog.LevelInfo)
	handler := baseHandler.WithAttrs(attrs)
	logger := slog.New(handler)

	logger.Info("a basic message", "info-key", "info-value")
	expected := []capslog.CapturedRecord{
		{
			Message: "a basic message",
			Level:   slog.LevelInfo,
			Attrs: []slog.Attr{
				{
					Key:   "info-key",
					Value: slog.StringValue("info-value"),
				},
				{
					Key:   "my-key",
					Value: slog.StringValue("value"),
				},
			},
		},
	}

	require.Equal(t, expected, baseHandler.CapturedRecords)
}

func TestWithGroups(t *testing.T) {
	baseHandler := capslog.NewCaptureHandler(slog.LevelInfo)
	handler := baseHandler.WithGroup("first-group").WithGroup("second-group")
	logger := slog.New(handler)

	logger.Info("a basic message")
	expected := []capslog.CapturedRecord{
		{
			Message: "a basic message",
			Level:   slog.LevelInfo,
			Groups:  []string{"first-group", "second-group"},
		},
	}

	require.Equal(t, expected, baseHandler.CapturedRecords)
}

func TestNullHandler(t *testing.T) {
	t.Run("never enabled", func(t *testing.T) {
		handler := &capslog.NullHandler{}
		for i := slog.LevelDebug; i <= slog.LevelError; i++ {
			t.Run("not enabled for "+i.String(), func(t *testing.T) {
				require.False(t, handler.Enabled(context.Background(), i))
			})
		}
	})

	t.Run("handles without error", func(t *testing.T) {
		handler := &capslog.NullHandler{}
		require.NoError(t, handler.Handle(context.Background(), slog.Record{}))
	})

	t.Run("returns new nullhandler for WithAttrs", func(t *testing.T) {
		handler := &capslog.NullHandler{}
		require.IsType(t, &capslog.NullHandler{}, handler.WithAttrs([]slog.Attr{}))
	})

	t.Run("returns new nullhandler for WithGroup", func(t *testing.T) {
		handler := &capslog.NullHandler{}
		require.IsType(t, &capslog.NullHandler{}, handler.WithGroup("my-group"))
	})
}
